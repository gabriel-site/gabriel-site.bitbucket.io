#!/bin/sh

# cu './' sau fara './' ?
ASSETS_DIR=./assets
SITE_DIR=site
CONTENT_DIR=./content

[ -d $SITE_DIR ] || mkdir $SITE_DIR

# parcurge recursiv fiecare folder/director si genereaza foldere si pagini .html
# corespunzatoare celor din directorul curent
in_dir() {
	# local petnru a nu se incurac in apelurile recursive
	local DIR=$1
	[ -d $DIR ] || return;
	echo "This dir=$DIR"
	# get dir name for $SITE_DIR
	local dir=$(echo $i | sed  -e "s|^.*$CONTENT_DIR\/||")
	echo "Dir=$SITE_DIR/$dir"
	[ -d $SITE_DIR/$dir ] || mkdir "$SITE_DIR/$dir"

	for i in $DIR/*; do
		[ -d $i ] && { in_dir $i; continue; }
		# aici nu trebuie local pentru ca sunt refacute la fiecare iteratie
		page=$(basename $i)
		# just copy non markdown files
		[ ${page##*.} = "md" ] || { cp $i "$SITE_DIR/$dir"; continue; }
		page_path="$SITE_DIR/$dir"
		echo "Page=$page in $page_path/$page"
		pandoc -s $i -o $page_path/${page%.*}.html --template $ASSETS_DIR/template.html
	done
}

for i in $CONTENT_DIR/* ;do
	page=$(basename $i)
	[ -d $i ] && { in_dir $i; continue; }
	echo "In main: $i"
	# echo ${page##*.}	# doar extensia
	# echo ${page%.*}	# fara extensie
	pandoc -s $i -o $SITE_DIR/${page%.*}.html --template $ASSETS_DIR/template.html
done
