---
title: Izolarea si limitarea resurselor proceselor
---

Aceasta este lucrarea mea de terminare a programului de licenta la universitatea
Bucuresti.

Licrarea poarta numele **IZOS**. De ce?  
... "izo" de la izolarea pe care o face intre procesele de pe sistemul de operare.

O versiune web a lucrari redactate este disponibila aici: [IZOS](izos.html)

Codul sursa este disponibil pe [Bitbucket](https://bitbucket.org/GabrielCirstea/izos)

Codul si documentatie se vor mai schimba deoarece intentionez sa continui proiectul
pana ajunge intr-un statiu functional si mai ales stabil.

## Scop

Lucrarea a fost una de cercetare si explorare a metodelor si tehnologiilor
folosite pentru manipularea si limitarea resurselor la care procesele ce sunt
executatea pe un computer au accest

Premiza lucrarii este ca astazi "computer-ele" sunt si continua sa devina din ce
in ce mai complexe, precum si toate programele/aplicatiile pe care utilizatorii
le folosesc.
Multe dintre acestea nu fac doar ceea ce pretind, iar altele au numai intentii
~rele~.

Scopul lucrarii mele a fost de a studia si de a incepe prototipul unei metode prin
care sa se poata limita resursele la care aplicatiile au acces.
Astfel oferind utilizatorului un mai bun control asupra prorpiului computer.
