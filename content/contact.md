---
title: Gabriel's site
---

### Mail

Cea mai de baza metroda

* <gabriel_cirstea@protonmail.com>
* <cirstea6@gmail.com>

### Matrix protocol

* @gabrielcirstea:matrix.org
<a href="https://matrix.org"><img class="logo_img"
src="/images/matrix-protocol-logo.png"></a>
<a href="https://element.io/"><img class="logo_img" src="/images/element-logo.png"></a>

### Open source

* [Github <img class="logo_img" src="/images/github-logo.png">](https://github.com/GabrielCirstea)
* [Gitlab <img class="logo_img" src="/images/gitlab-logo.png">](https://gitlab.com/GabrielCirstea)
* [Bitbucket <img class="logo_img" src="/images/bitbucket-logo.png">](https://bitbucket.org/GabrielCirstea)

In cazul in care o sa invep a folosi si alte platforme (precum tentativa de
[mastodon](https://joinmastodon.org) o sa completez aici.
