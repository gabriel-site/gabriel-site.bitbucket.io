---
title: Gabriel's site
---

## Pasiuniile mele

Sunt pasionat de diverse protocoale de comunicare si de securitate,
open source este un concept foarte puternic, pe care il favorizez,
intrucat imi permite sa vad cum este implementat un progra,
algoritm sau protocol
Unele din proiectele mele favorite gasite online care mi-au starnit
interesul:

### [ZeroNet](https://zeronet.io/) 
<a href="https://github.com/HelloZeroNet/ZeroNet">
<img class="logo_img" src=/images/github-logo.png></a>

![ZeroNet](/images/zeroNet.jpeg)

O platforma/protocol de comunicare peer-to-peer ce permite
utilizatoriilor sa creeze, partajeze si acceseze site-uri si
informatii, fara a fi nevoie de un server central.
Acest proiect este pe scurt un alt fel de internet decentralizat,
fiecare este liber sa isi faca propriul spatiu in care sa
se desfasoare, sau poate folosi site-urile/spatiile facute de altii.
<div class="inaltator"></div>

### [Matrix](https://matrix.org/)  

Un protocol deschis (open) de comunicare sigur, decentralizat in timp real.
Matrix mi-a atras atentia prin felul in care este contruit:
*sa fie decentralizat*. Spre deosebire de alte protocoale
sau "aplicatii" folosite astazi (discord, skype, slack...)
matrix este decentralizat, exista mai multe servere (servers)
la cere utilizatorii isi pot face cont si pot comunica cu alti utilizatori
indiferent de server-ul ales.
Pe scurt e ca la email: cu o adresa de gmail
se poate comunica cu utilizator de pe yahoo, outlook etc.  
De asemenea matrix are si notiunea de "bridge" pentru servere.
Un "bridge" permite comunicarea cu alt protocol.
Un simplu exemplu ar fi: crearea unei camere pe matrix in care
sa fie prezenti si utilizatori de discord, telegram, slack etc.  
Matrix este doar protocolul, este open-standard, si sunt
prezente mai multe inplementari pentru server. De asemenea
exista mai multe aplicatii de tip client, cea mai populara
fiind <a href="https://element.io">Element
<img class="logo_img" src="/images/element-logo.png"></a>

O descriere a notiunii de retea deschisa ("open network") de Bram van den Heuvel:
[in acest video](https://www.youtube.com/watch?v=v5Y8zCwIxjI) sau pe [site](https://noordstar.me/b/decentralize.md)

### [Wireguard](https://www.wireguard.com/)
<img class="logo_img" src="/images/wireguard-logo.png"></a>

Wireguard este un protocol de
[VPN](https://en.wikipedia.org/wiki/Virtual_private_network) (Virtual Private
Network) diferit de celelalte precum [OpenVPN](https://openvpn.net/) prin
simplitatea implementarii.  Wireguard este mult mai simplu, mai rapid si totusi
foloseste protocoale de criptare moderne, bine puse la punct.

Wireguard este acum integrat in kernelul de linux ceea ce ii ofera un avantaj
cand vine vorba de performanta.
Iar modul de interactiune este printr-o interfata de retea virtuala ce poate fi
usor gestionata cu alte programe pentru *routing* sau *firewall*.

Unul din scopurile principale ale proiectului este de a fi simplu de instalat si
de folosit. Aici pot sa spun ca proiectul chiar si-a atins obiectivul.
Se poate seta foarte usor, am instalat serverul de VPN pentru un [proiect la
facultate](https://github.com/GabrielCirstea/ScripturiRetele/tree/master/VPN_experimet)
a fost super rapid, in pricipal se urmeaza instructiuniile de pe
[site](https://www.wireguard.com/install/)
