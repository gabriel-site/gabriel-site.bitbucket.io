---
title: Gabriel's site
---

# Wireguard

Wireguard este un protocol de
[VPN](https://en.wikipedia.org/wiki/Virtual_private_network) (Virtual Private
Network) diferit de celelalte precum [OpenVPN](https://openvpn.net/) prin
simplitatea implementarii.  Wireguard este mult mai simplu, mai rapid si totusi
foloseste protocoale de criptare moderne, bine puse la punct.

Wireguard este acum integrat in kernelul de linux ceea ce ii ofera un avantaj
cand vine vorba de performanta.
Iar modul de interactiune este printr-o interfata de retea virtuala ce poate fi
usor gestionata cu alte programe pentru *routing* sau *firewall*.

# NetMaker

NetMaker este un sistem de automatizare a procesului de setare si intretinere a
unui VPN wireguard. Ce ofera posibilitatea realizarii unei VPN cross cloud,
folosind kebernetes. O retea intre dispozitivele IoT pentru securizarea comunicarii.

Acest proiect ofera posibilitatea setarii rutelor intre servere, DNS si noduri
de backup.

Utilizari principale ale proiectului:

* Kubereneste: posibilitatea realizarii unui tunel de VPN (wireguard) cross cloud.
Netmaker va crea interfete de retea de tip wireguard pe fiecare node si le va
adauga in VPN
* Realizarea conexiunii de tip **bridge** intre mai multe retele VPN
* Realizarea unei conexiuni private peste internet, ce incorporeaza un server de
DNS, la care se vor adauga mai multi clienti/utilizatori
