---
title: Gabriel's site
---

<section>
<div class="img_holder">
<img id="profil" src="/images/Profil.png">
</div>
</section>

## Despre mine

Am terminat un program de licenta la facultatea de Matematica si
Informatica in cadrul Universitatii din Bucuresti. Unde am invatat
si mi-am dezvoltat abilitatii legate de programare si de lucru cu
diverse concepte de algoritmica, lucru in echipa, organizarea proiectelor
si masura petrecerilor in "Centru vechi".

Acum am intrat la programul de master "Security and applied logic"
in cadrul acceleasi facultati.

### [Licenta](licenta/)

Lucrarea de finalizare a studiilor de licenta in cadrul facultatii de
Matematica si informatica a fost orientata pe gestionarea resurselor la care
procesele de pe computer au acces.

O prezentare mai detaliata [aici](licenta/)

## Ce ma atrage?

Sunt interesat de tehnologiile de comunicare, diverse protocoale
ce stau la baza comunicarii pe internet si securitate
Ma pasioneaza foarte mult IPv6 pentru flexibilitatea si beneficiile
pe care le aduce celor ce vor sa incerce sa foloseasca diverse tehnologi,
in special tehnologi Peer-to-Peer.

## Cu ce stiu sa lucrez?

Pe scurt, am facut cate ceva:

* Programare in C/C++ (in special C pe sisteme Linux)
* Cateva teme in JavaScript
* Anumite proiecte in Python
* Bash scripting
* Html, CSS
* Latex
* SQL

De asemenea am lucrat si cu..

* R
* Matlab (octave)

## Stadiul site-ului

(Aceasta sectiune ar trebui mutat altundeva)

Am inceput sa lucrez la site folosind [**pandoc**](https://pandoc.org) si niste simplu *bash scripting*
pentru a avea un fel de "static html site generator".

Scriptul este foarte simplu, doar ma sjuta sa pastrez fisierele aranjate in anumite
foldere si foloseste **pandoc** pe un template personalizat pentru a genera paginile.

Avantaje:

* scriu in markdown
	* mai simplu decat html
	* chiar si cu keybindings pentru html, tot poate fi enervant.
* mai usor de lucrat cu meniul de navigare
	* am pus meniul in tamplate, daca vreau sa adaug sau sa sterg intrari din meniu
trebuie sa o fac doar o data.
* inca nu am folosit, dar ma astept sa ma ajute mult cand o sa lucrez cu mai
multe liste sau tabele.

Dezavantaje:

* nu pot sa setez clasele si id-urile elementelor de html
* nu pot sa creez usor div-uri
	* in markdown pot folosi cod html, pentru a insera imagini de anumite
dimensiuni sau elemente care sa aibe anumite prorpietati.
